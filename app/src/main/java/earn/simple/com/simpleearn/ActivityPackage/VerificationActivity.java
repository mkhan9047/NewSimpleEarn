package earn.simple.com.simpleearn.ActivityPackage;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import earn.simple.com.simpleearn.R;

public class VerificationActivity extends AppCompatActivity {

    EditText first, second, third, fourth, fifth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_varification);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }


        InitView();

        ChangeFocus();
    }


    public void onVerify(View view) {

        if (NotEmpty(first, second, third, fourth, fifth)) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(this, "First Enter the whole number", Toast.LENGTH_SHORT).show();
        }

    }

    private void getVerified(int code, int ref) {
/*

        NetworkInterface networkInterface = RetrofitBuilder.getRetrofit().create(NetworkInterface.class);
        Call<VerifyMail> call = networkInterface.getEmailVerified(code, ref);
        call.enqueue(new Callback<VerifyMail>() {
            @Override
            public void onResponse(Call<VerifyMail> call, Response<VerifyMail> response) {
                VerifyMail mail = response.body();

            }

            @Override
            public void onFailure(Call<VerifyMail> call, Throwable t) {

            }
        });
*/

    }


    private void ChangeFocus() {
        first.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 1) {
                    second.requestFocus();
                    if (second.requestFocus()) {
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    }
                }
            }
        });

        second.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 1) {
                    third.requestFocus();
                    if (third.requestFocus()) {
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    }
                }
            }
        });

        third.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 1) {
                    fourth.requestFocus();
                    if (fourth.requestFocus()) {
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    }
                }
            }
        });

        fourth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 1) {
                    fifth.requestFocus();
                    if (fifth.requestFocus()) {
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    }
                }
            }
        });
    }


    private void InitView() {
        first = findViewById(R.id.verify_first);
        second = findViewById(R.id.verify_second);
        third = findViewById(R.id.verify_third);
        fourth = findViewById(R.id.verify_fourth);
        fifth = findViewById(R.id.verify_fifth);
    }


    private int Marger(EditText[] texts) {

        StringBuilder builder = new StringBuilder();

        for (EditText data : texts) {
            builder.append(data.getText().toString());
        }

        return Integer.parseInt(builder.toString());
    }

    private boolean NotEmpty(EditText first, EditText Second, EditText third, EditText fourth, EditText fifth) {

        if (first.getText().toString().length() < 0 && Second.getText().toString().length() < 0
                && third.getText().toString().length() < 0 && fourth.getText().toString().length() < 0 && fifth.getText().toString().length() < 0
                ) {

            return false;

        } else {

            return true;
        }


    }

}
