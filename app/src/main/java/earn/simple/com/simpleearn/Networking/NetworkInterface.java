package earn.simple.com.simpleearn.Networking;


import earn.simple.com.simpleearn.ModelPackage.CreateUser;
import earn.simple.com.simpleearn.ModelPackage.SignIn;
import earn.simple.com.simpleearn.ModelPackage.SignUp;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface NetworkInterface {

    @FormUrlEncoded
    @POST("api/sign-in")
    Call<SignIn> DoSignIN(@Field("email")String email, @Field("password")String password);


    @POST("api/sign-up")
    Call<SignUp> DoSignUp(@Body() CreateUser createUser);


}
