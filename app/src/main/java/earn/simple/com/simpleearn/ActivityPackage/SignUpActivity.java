package earn.simple.com.simpleearn.ActivityPackage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import earn.simple.com.simpleearn.Helper.Utility;
import earn.simple.com.simpleearn.ModelPackage.CreateUser;
import earn.simple.com.simpleearn.ModelPackage.SignUp;
import earn.simple.com.simpleearn.Networking.NetworkInterface;
import earn.simple.com.simpleearn.Networking.RetrofitBuilder;
import earn.simple.com.simpleearn.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SignUpActivity extends AppCompatActivity {

    EditText name, email, password, confirmPassword, phone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initView();

    }

    public void onSignUp(View view) {

        boolean shouldCall = HandleInputError(GetEmail(email), GetPassword(password), GetName(name), GetConfirmPassword(confirmPassword), GetPhone(phone));

        if (shouldCall) {

            if (Utility.EmailValidator(GetEmail(email))) {

                SignUpResult();

            } else {

                Toast.makeText(this, "Email is not valid!", Toast.LENGTH_SHORT).show();

            }

        }

    }


    public void initView() {

        name = findViewById(R.id.signup_name);
        email = findViewById(R.id.signup_email);
        password = findViewById(R.id.signup_password);
        confirmPassword = findViewById(R.id.signup_confirmpassword);
        phone = findViewById(R.id.signup_phone);

    }


    private void SignUpResult() {
        /*network call here*/
        NetworkInterface networkInterface = RetrofitBuilder.getRetrofit().create(NetworkInterface.class);
        Call<SignUp> signUpCall = networkInterface.DoSignUp(new CreateUser("echosoft240@gmail.com", "mkhan7800", "mkhan7800",

                "01952454545", "12345678", "2"

        ));

        signUpCall.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {

                Log.d("signUPResult", response.body().toString());

            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {

            }
        });
    }


    private String GetEmail(EditText email) {

        if (email.getText().toString().length() == 0) {


            return null;

        } else {

            return email.getText().toString();
        }

    }

    private String GetPhone(EditText phone) {

        if (phone.getText().toString().length() == 0) {


            return null;

        } else {

            return phone.getText().toString();
        }

    }

    private String GetPassword(EditText password) {

        if (password.getText().toString().length() == 0) {


            return null;

        } else {

            return password.getText().toString();
        }
    }

    private String GetName(EditText name) {

        if (name.getText().toString().length() == 0) {

            return null;

        } else {

            return name.getText().toString();
        }
    }

    private String GetConfirmPassword(EditText confirmPassword) {

        if (confirmPassword.getText().toString().length() == 0) {

            return null;

        } else {

            return confirmPassword.getText().toString();
        }
    }

    private boolean HandleInputError(String email, String password, String name, String confirmPassword, String phone) {

        if (email == null && password == null && name == null && confirmPassword == null) {

            Toast.makeText(this, "Please Input Email, password, name and confirm password!", Toast.LENGTH_SHORT).show();

            return false;

        } else if (email == null) {

            Toast.makeText(this, "Please Input Email!", Toast.LENGTH_SHORT).show();

            return false;

        } else if (password == null) {

            Toast.makeText(this, "Please Input Password!", Toast.LENGTH_SHORT).show();

            return false;

        } else if (name == null) {

            Toast.makeText(this, "Please Input your Name!", Toast.LENGTH_SHORT).show();

            return false;

        } else if (!password.equals(confirmPassword)) {

            Toast.makeText(this, "Password and Confirm password doesn't match!", Toast.LENGTH_SHORT).show();

            return false;

        } else if (phone == null) {

            Toast.makeText(this, "Please Enter Phone Number!", Toast.LENGTH_SHORT).show();

            return false;
        }


        return true;
    }


}
