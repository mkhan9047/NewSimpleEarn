package earn.simple.com.simpleearn.FragmentPackage;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import earn.simple.com.simpleearn.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LevelViewFragment extends Fragment {

    TextView l_one, l_two, l_three, l_four, l_five;
    TextView l_six, l_seven;

    TextView inactive, agents;

    public LevelViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_level_view, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    private void InitView(){
        View view = getView();
        if(view != null){
            l_one = view.findViewById(R.id.level_one);
            l_two = view.findViewById(R.id.level_two);
            l_three = view.findViewById(R.id.level_three);
            l_four = view.findViewById(R.id.level_four);
            l_five = view.findViewById(R.id.level_five);
            l_six = view.findViewById(R.id.level_six);
            l_seven = view.findViewById(R.id.level_seven);
            
        }
    }
}
