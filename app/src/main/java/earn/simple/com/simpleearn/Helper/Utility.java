package earn.simple.com.simpleearn.Helper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utility {

    public static ProgressDialog StartProgressDialog(Context context, String message) {

        ProgressDialog dialog = new ProgressDialog(context);

        ((Activity) context).getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,

                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        dialog.setIndeterminate(true);

        dialog.setMessage(message);

        dialog.setCanceledOnTouchOutside(false);

        dialog.show();

        return dialog;
    }

    public static void DismissDialog(ProgressDialog dialog, Context context) {

        if (dialog.isShowing()) {

            dialog.dismiss();

            ((Activity) context).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    public static void ShowToast(Context context, String message){

        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

    }


    public static boolean EmailValidator(String email){

        String regex = "^[\\p{L}\\p{N}\\._%+-]+@[\\p{L}\\p{N}\\.\\-]+\\.[\\p{L}]{2,}$";

        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(email);

        return matcher.matches();

    }


}
