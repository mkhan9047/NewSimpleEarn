package earn.simple.com.simpleearn.FragmentPackage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import earn.simple.com.simpleearn.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFrgament extends Fragment {


    public DashboardFrgament() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard_frgament, container, false);
    }

}
