package earn.simple.com.simpleearn.Networking;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitBuilder {

    private static final String baseURL = "https://debucse11.000webhostapp.com/";

    private static Retrofit retrofit = null;

    public static Retrofit getRetrofit(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        if(retrofit == null){

            retrofit = new Retrofit.Builder().baseUrl(baseURL).
                    client(client).
                    addConverterFactory(GsonConverterFactory.create()).build();
        }

        return retrofit;
    }
}
