package earn.simple.com.simpleearn.ActivityPackage;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import earn.simple.com.simpleearn.Helper.Utility;
import earn.simple.com.simpleearn.ModelPackage.SignIn;
import earn.simple.com.simpleearn.Networking.NetworkInterface;
import earn.simple.com.simpleearn.Networking.RetrofitBuilder;
import earn.simple.com.simpleearn.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LogInActivity extends AppCompatActivity {

    EditText email, password;
    AppCompatButton signInButton;
    TextView forgotPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        /*type casting all the views*/
        InitView();

    }


    private void InitView() {

        email = findViewById(R.id.signin_email);
        password = findViewById(R.id.signin_password);
        signInButton = findViewById(R.id.btn_signin);
        forgotPassword = findViewById(R.id.forgot_password_txt_btn);

    }


    public void onSignIn(View view) {
        /*Handle input*/

        boolean shouldCall = HandleInputError(GetEmail(email), GetPassword(password));

        if (shouldCall) {

            if (Utility.EmailValidator(GetEmail(email))) {

                SignInCall(GetEmail(email), GetPassword(password));

            } else {

                Toast.makeText(this, "Email is not valid!", Toast.LENGTH_SHORT).show();

            }


        }

    }


    private void SignInCall(String email, String password) {

        /*start progress dialog*/
        final ProgressDialog dialog = Utility.StartProgressDialog(this, "Signing In...");

        NetworkInterface networkInterface = RetrofitBuilder.getRetrofit().create(NetworkInterface.class);

        Call<SignIn> signInCall = networkInterface.DoSignIN(email, password);

        signInCall.enqueue(new Callback<SignIn>() {
            @Override
            public void onResponse(Call<SignIn> call, Response<SignIn> response) {

                SignIn signIn = response.body();

                if (signIn != null) {

                    Log.d("sign_in: ", signIn.toString());

                    if (signIn.isSuccess()) {

                        Intent intent = new Intent(LogInActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();

                    } else if (!signIn.isSuccess()) {

                        Utility.ShowToast(LogInActivity.this, signIn.getMessage());
                    }

                    Utility.DismissDialog(dialog, LogInActivity.this);
                }

            }

            @Override
            public void onFailure(Call<SignIn> call, Throwable t) {

                Log.d("NetworkError", t.getMessage());
                Utility.DismissDialog(dialog, LogInActivity.this);
            }
        });

    }

    private String GetEmail(EditText email) {

        if (email.getText().toString().length() == 0) {


            return null;

        } else {

            return email.getText().toString();
        }

    }



    private String GetPassword(EditText password) {

        if (password.getText().toString().length() == 0) {


            return null;

        } else {

            return password.getText().toString();
        }
    }

    private boolean HandleInputError(String email, String password) {

        if (email == null && password == null) {

            Toast.makeText(this, "Please Input Email And Password!", Toast.LENGTH_SHORT).show();

            return false;

        } else if (email == null) {

            Toast.makeText(this, "Please Input Email!", Toast.LENGTH_SHORT).show();

            return false;

        } else if (password == null) {

            Toast.makeText(this, "Please Input Password!", Toast.LENGTH_SHORT).show();

            return false;

        }

        return true;
    }

    public void onCreateAccount(View view) {

        Intent intent = new Intent(this, SignUpActivity.class);

        startActivity(intent);
    }
}
