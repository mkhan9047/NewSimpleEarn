package earn.simple.com.simpleearn.Stroage;

import android.content.Context;
import android.content.SharedPreferences;

public class LocalStorage {

    private static final boolean LOGIN_SATE = false;


    private Context context;

    public LocalStorage(Context context) {
        this.context = context;
    }


    private SharedPreferences.Editor getPreferencesEditor() {

        return getsharedPreferences().edit();
    }

    private SharedPreferences getsharedPreferences() {

        return context.getSharedPreferences("MyData", Context.MODE_PRIVATE);
    }

    public void SaveLogInSate(boolean p) {

        getPreferencesEditor().putBoolean("logged_in", p).commit();

    }

    public boolean getLogInState() {

        return getsharedPreferences().getBoolean("logged_in", LOGIN_SATE);
    }


}
