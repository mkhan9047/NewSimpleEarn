package earn.simple.com.simpleearn.ModelPackage;

public class CreateUser {

    private String email;
    private String password;
    private String password_confirmation;
    private String phone;
    private String reference_code;
    private String role;

    public CreateUser(String email, String password, String password_confirmation, String phone, String reference_code, String role) {
        this.email = email;
        this.password = password;
        this.password_confirmation = password_confirmation;
        this.phone = phone;
        this.reference_code = reference_code;
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getPassword_confirmation() {
        return password_confirmation;
    }

    public String getPhone() {
        return phone;
    }

    public String getReference_code() {
        return reference_code;
    }

    public String getRole() {
        return role;
    }
}
